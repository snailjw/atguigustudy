### NIO通道的数据传输与内存映射文件

[TOC]

#### 1.通道（Channel）

通道用于源节点与目标节点的连接，在Java NIO中负责缓冲区中数据的传输，Channel本身不存储数据，因此需要配合缓冲区进行传输

#### 2.通道的主要实现类

java.nio.channels.Channel 接口

* FileChannel
* SocketChannel
* ServerSocketChannel
* DatagramChannel

#### 3.获取通道

1. Java针对支持通道的类提供了getChannel()方法

   本地IO：FileInputStrem/FileOutputStream/RandomAccessFile

   网络IO：Socket/ServerSocket/DatagramSocket

2. 在JDK1.7中的NIO.2针对各个通道提供了静态方法open()

3. 在JDK1.7中的NIO.2的Files工具类的newByteChannel()

